﻿#include <chrono>
#include <iostream>
#include <utility>
#include <vector>
#include "result.cpp"
#include "custom.h"

Result<Custom1> make_custom1(int i) {
    if(i % 10 == 0) {
        return Result<Custom1>::error(1);
    }
    return Result<Custom1>::ok(Custom1(i));
}

int main() {

    std::vector<Custom1*> customs;

    // 時間計測開始
    auto start = std::chrono::system_clock::now();

    for(int i = 0; i < 100; i++) {
        Result<Custom1> custom1 = make_custom1(i);
        if(!custom1.isOk()) {
            std::cout << i << ": Error(" << custom1.getStatusCode() << ")" << std::endl;
            continue;
        }
        
        Custom1* org = custom1.getValue();
        Custom1* copied = new Custom1(std::move(*org)); //orgは破棄されてしまうので再アロケート
        customs.push_back(copied);
    }

    // 時間計測終了
    auto end = std::chrono::system_clock::now();
    auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(end.time_since_epoch())
        - std::chrono::duration_cast<std::chrono::milliseconds>(start.time_since_epoch());

    for(int i = 0; i < customs.size(); i++) {
        std::cout << "Value(" << customs[i]->getInnerValue() << ")" << std::endl;
    }

    std::cout << "duration: " << ms.count() << " ms" << std::endl;

    return 0;
}
