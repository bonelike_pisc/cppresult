#include <iostream>
#include "custom.h"

Custom1::Custom1(int inner_val) {
    for(int i = 0; i < Custom1::SIZE; i++) {
        this->inner_value.push_back(inner_val + i);
    }
}

Custom1::Custom1(Custom1& other) : inner_value(other.inner_value) {
    //std::cout << "copy occured." << std::endl;
}
Custom1::Custom1(Custom1&& other) : inner_value(other.inner_value) {
    //std::cout << "move occured." << std::endl;
}

int Custom1::getInnerValue() const {
    return this->inner_value[0];
}
