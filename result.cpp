﻿#include <utility>

// 簡易的な、データとエラーのいずれかを取得するためのクラス
template <typename T>
class Result {
public:
    static Result<T> ok(T&& value); //正常な結果
    static Result<T> error(int code); //エラー
    bool isOk() const;
    T* getValue() const; //結果を取得する
    const int getStatusCode() const;
    Result(Result<T>& other) = delete; //コピーは禁止
    Result(Result<T>&& other);
    ~Result();
private:
    T* value;
    int status;
    Result(T* value, int status);
};

template <typename T>
Result<T>::Result(T* value, int status)
    : value(value), status(status) {}

template <typename T>
Result<T>::Result(Result<T>&& other) : value(other.value), status(other.status) {
    //other.valueの指し先がデストラクタによって破棄されるのを防ぐ
    other.value = nullptr;
}

template <typename T>
Result<T>::~Result() {
    if(this->value != nullptr) {
        delete this->value;
    }
}

template <typename T>
Result<T> Result<T>::ok(T&& value) {
    return Result(new T(std::move(value)), 0);
}

template <typename T>
Result<T> Result<T>::error(int code) {
    if (code == 0) {
        throw "Error code must be NOT 0.";
    }
    return Result(nullptr, code);
}

template <typename T>
bool Result<T>::isOk() const {
    return this->status == 0;
}

template <typename T>
T* Result<T>::getValue() const {
    if(this->status != 0) {
        throw "Result is not OK.";
    }
    return this->value;
}

template <typename T>
const int Result<T>::getStatusCode() const {
    return this->status;
}
