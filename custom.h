#ifndef __CUSTOM_H__
#define __CUSTOM_H__

#include <vector>

class Custom1 {
public:
    Custom1(int inner_val);
    Custom1(Custom1& other);
    Custom1(Custom1&& other);
    int getInnerValue() const;
private:
    const static int SIZE = 1000000;
    std::vector<int> inner_value;
};

#endif
